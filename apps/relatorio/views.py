# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from ..fornecedor.models import *

def por_segmento(request, segmento):
    segmentos = Segmento.objects.filter(status=True)
    fornecedores = Fornecedor.objects.filter(status=True).filter(segmento=segmento)

    return render(request, 'por_segmento.html', {'fornecedores': fornecedores, 'segmentos': segmentos})

def por_segmento_geral(request):
    segmentos = Segmento.objects.filter(status=True)
    fornecedores = Fornecedor.objects.filter(status=True)

    return render(request, 'por_segmento.html', {'fornecedores': fornecedores, 'segmentos': segmentos})