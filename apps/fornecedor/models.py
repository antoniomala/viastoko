# -*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify
from django.db.models import Sum

class Fornecedor(models.Model):
    cnpj = models.CharField(u'CNPJ', max_length=50, unique=True)
    nm_fantasia = models.CharField(u'Nome fantasia', max_length=300)
    uf = models.CharField(u'Estado', max_length=2)
    cidade = models.CharField(u'Cidade', max_length=150)
    responsavel = models.CharField(u'Responsável', max_length=(300))
    telefone = models.CharField(u'Telefone', max_length=30)
    segmento = models.ForeignKey('Segmento')
    slug = models.SlugField(max_length=400, blank=True, null=True, editable=False)

    # Fields de auditoria
    status = models.BooleanField(u'Ativo?', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super(Fornecedor, self).save(*args, **kwargs)
        slug = "%s-%s" % (slugify(self.nm_fantasia), self.id)
        if not self.slug or self.slug != slug:
            self.slug = slug
            self.save()

    class Meta:
        db_table = 'fornecedores'
        verbose_name = u'Fornecedor'
        verbose_name_plural = u'Fornecedores'

    def __str__(self):
        return self.nm_fantasia

    def __unicode__(self):
        return self.nm_fantasia

    def pontuacao_total(self):
        from ..fornecimento.models import Fornecimento
        return Fornecimento.objects.filter(fornecedor_id=self.id).aggregate(
            pontuacao_total=Sum('quantidade')
                            + Sum('prazo')
                            + Sum('qualidade')
                            + Sum('integridade')
                            + Sum('nota_fiscal'))['pontuacao_total']


class Segmento(models.Model):
    descricao = models.CharField(u'Descrição', max_length=300)

    # Fields de auditoria
    status = models.BooleanField(u'Ativo?', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'segmento'
        verbose_name = u'Segmento'
        verbose_name_plural = u'Segmentos'

    def __str__(self):
        return self.descricao

    def __unicode__(self):
        return self.descricao