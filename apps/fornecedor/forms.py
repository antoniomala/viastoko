# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from .models import *


class FornecedorForm(ModelForm):
    class Meta:
        model = Fornecedor
        exclude = ['slugify']
        widgets = {
            'cnpj' : forms.TextInput(attrs = {'class' : 'form-control', 'placeholder': u'99.999.999/9999-99'}),
            'nm_fantasia': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Ex: Alimentos S/A'}),
            'cidade': forms.Select(attrs={'class': 'form-control', 'placeholder': u'Ex: Recife'}),
            'uf': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Ex: PE'}),
            'responsavel': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Ex: João Silva'}),
            'telefone': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'(99) 9999-9999'}),
            'segmento': forms.Select(attrs={'class': 'form-control', 'placeholder': u'Alimentos de origem animal'}),
        }


class SegmentoForm(ModelForm):
    class Meta:
        model = Segmento
        exclude = ['status']
        widgets = {
            'descricao': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Alimentos de origem animal'}),
        }