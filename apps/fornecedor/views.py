# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import *
from django.contrib.auth.decorators import login_required


# Fonecedores
@login_required(login_url='/entrar/')
def cadastrar_fornecedor(request):
    form = FornecedorForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_fornecedores))

    return render(request, 'cadastrar_fornecedor.html', {'form': form, 'request': request})


@login_required(login_url='/entrar/')
def lista_fornecedores(request):
    fornecedores = Fornecedor.objects.filter(status=True)
    return render(request, 'fornecedores.html', {'fornecedores': fornecedores})


@login_required(login_url='/entrar/')
def fornecedores_cancelados(request):
    fornecedores = Fornecedor.objects.filter(status=False)
    return render(request, 'fornecedores_cancelados.html', {'fornecedores': fornecedores})


@login_required(login_url='/entrar/')
def exclui_fornecedor(request, id):
    fornecedor = Fornecedor.objects.get(pk=id)
    fornecedor.status = False
    fornecedor.save()
    return HttpResponseRedirect(reverse(lista_fornecedores))


@login_required(login_url='/entrar/')
def ativar_fornecedor(request, id):
    fornecedor = Fornecedor.objects.get(pk=id)
    fornecedor.status = True
    fornecedor.save()
    return HttpResponseRedirect(reverse(fornecedores_cancelados))


@login_required(login_url='/entrar/')
def editar_fornecedor(request, id):
    fornecedor = Fornecedor.objects.get(pk=id)
    form = FornecedorForm(request.POST or None, instance=fornecedor)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_fornecedores))
    return render(request, 'editar_fornecedor.html', {'form': form, 'fornecedor': fornecedor})



# Segmentos
@login_required(login_url='/entrar/')
def cadastrar_segmento(request):
    form = SegmentoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_segmentos))

    return render(request, 'cadastrar_segmento.html', {'form': form, 'request': request})


@login_required(login_url='/entrar/')
def lista_segmentos(request):
    segmentos = Segmento.objects.filter(status=True)
    return render(request, 'segmentos.html', {'segmentos': segmentos})


@login_required(login_url='/entrar/')
def segmentos_cancelados(request):
    segmentos = Segmento.objects.filter(status=False)
    return render(request, 'segmentos_cancelados.html', {'segmentos': segmentos})


@login_required(login_url='/entrar/')
def exclui_segmento(request, id):
    segmento = Segmento.objects.get(pk=id)
    segmento.status = False
    segmento.save()
    return HttpResponseRedirect(reverse(lista_segmentos))


@login_required(login_url='/entrar/')
def ativar_segmento(request, id):
    segmento = Segmento.objects.get(pk=id)
    segmento.status = True
    segmento.save()
    return HttpResponseRedirect(reverse(segmentos_cancelados))


@login_required(login_url='/entrar/')
def editar_segmento(request, id):
    segmento = Segmento.objects.get(pk=id)
    form = SegmentoForm(request.POST or None, instance=segmento)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_segmentos))
    return render(request, 'editar_segmento.html', {'form': form})
