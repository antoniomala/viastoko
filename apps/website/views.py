# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required(login_url='/entrar/')
def home(request):
    return render(request, 'home.html', {})

@login_required(login_url='/entrar/')
def dashboard(request):
    return render(request, 'dashboard.html', {})