# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from .models import *
from django.contrib.auth.decorators import login_required


@login_required(login_url='/entrar/')
def cadastrar_usuario(request):
    form = UsuarioForm(request.POST or None)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'cadastrar_usuario.html', {'form': form})


@login_required(login_url='/entrar/')
def listar_usuarios(request):
    usuarios = User.objects.filter(is_active=True)
    return render(request, 'listar_usuarios.html', {'usuarios': usuarios})


@login_required(login_url='/entrar/')
def usuarios_cancelados(request):
    usuarios = User.objects.filter(is_active=False)
    return render(request, 'usuarios_cancelados.html', {'usuarios': usuarios})


@login_required(login_url='/entrar/')
def exclui_usuario(request, usuario_id):
    usuario = User.objects.get(pk=usuario_id)
    usuario.is_active = False
    usuario.save()
    return HttpResponseRedirect(reverse(listar_usuarios))


@login_required(login_url='/entrar/')
def editar_informacoes(request, id):
    usuario = User.objects.get(pk=id)
    form = UsuarioForm(request.POST or None, instance=usuario)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('home'))

    return render(request, 'editar_informacoes.html', {'form': form})


# login no sistema
def entrar(request):
    # checando se o usuário já realizou o login e tomanda a decisão adequada
    if request.user.is_authenticated():
        if request.user.is_superuser:
            return HttpResponseRedirect(reverse('administracao'))
        else:
            return HttpResponseRedirect(reverse('home'))

    form = EntrarForm(request.POST or None)
    erro = False

    if request.method == 'POST':
        usuario = authenticate(username=request.POST['username'], password=request.POST['password'])

        if usuario is not None and usuario.is_active:
            login(request, usuario)

            # redirecionar para local adequado
            if usuario.is_superuser:
                return HttpResponseRedirect('/')
            else:
                # redireciona para área do cliente
                return HttpResponseRedirect('/')

        else:
            erro = True

    return render(request, 'entrar.html', {'form': form, 'erro': erro})


# logout no sistema
def sair(request):
    logout(request)
    return HttpResponseRedirect('/')

