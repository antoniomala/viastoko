# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import *
from django.contrib.auth.decorators import login_required


@login_required(login_url='/entrar/')
def cadastrar_fornecimento(request):
    form = FornecimentoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_fornecimentos))

    return render(request, 'cadastrar_fornecimento.html', {'form': form, 'request': request})


@login_required(login_url='/entrar/')
def lista_fornecimentos(request):
    fornecimentos = Fornecimento.objects.filter(status=True)
    return render(request, 'lista_fornecimentos.html', {'fornecimentos': fornecimentos})

