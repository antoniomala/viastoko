# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from ..fornecedor.models import Fornecedor
from ..unidade_gestora.models import UnidadeGestora

from datetime import datetime
from django.utils.dateformat import DateFormat
from django.utils.formats import get_format


class Fornecimento(models.Model):
    QUANTIDADE_CHOICES = (
        (30, u'Correta'),
        (28, u'Maior que o Solicitado'),
        (22, u'Maior ou igual a 75%'),
        (10, u'Maior que 50%, menor que 75%'),
        (0, u'Maior menor que 50%'),
    )

    PRAZO_CHOICES = (
        (30, u'Na data agendada'),
        (28, u'Fora do agendamento mas dentro da tolerancia'),
        (22, u'Até 15 dias de atraso'),
        (10, u'Até 30 dias de atraso'),
        (0, u'Mais que 30 dias de atraso'),
    )

    QUALIDADE_CHOICES = (
        (30, u'Aprovação total'),
        (28, u'Aprovação com ressalva(Baixa criticidade)'),
        (22, u'Aprovação com ressalva(Alta criticidade)'),
        (0, u'Reprovação'),
    )

    INTEGRIDADE_CHOICES = (
        (10, u'Aprovação total'),
        (5, u'Aprovação com ressalva'),
        (0, u'Reprovação'),
    )

    NOTA_FISCAL_CHOICES = (
        (10, u'NF OK'),
        (0, u'NF com problemas'),
    )

    DOCUMENTACAO_CHOICES = (
        (10, u'Documentos OK'),
        (0, u'Documentos com problemas'),
    )

    RECEBEU_CHOICES = (
        (u'Sim', u'Sim'),
        (u'Não', u'Não'),
    )

    ug = models.ForeignKey(UnidadeGestora)
    fornecedor = models.ForeignKey(Fornecedor)
    data = models.DateField(u'Data Pedido/Entrega', max_length=20)
    empenho = models.CharField(u'Identificação do pedido', max_length=200)
    nf = models.CharField(u'Numero da nota Fiscal', max_length=200)

    quantidade = models.IntegerField(u'Quantidade', choices=QUANTIDADE_CHOICES)
    prazo = models.IntegerField(u'prazo', choices=PRAZO_CHOICES)
    qualidade = models.IntegerField(u'Qualidade', choices=QUALIDADE_CHOICES)
    integridade = models.IntegerField(u'Integridade do produto', choices=INTEGRIDADE_CHOICES)
    nota_fiscal = models.IntegerField(u'Integridade da NF', choices=NOTA_FISCAL_CHOICES)
    obs = models.TextField(u'Obs', null=True, blank=True)
    recebeu = models.CharField(u'Recebido', choices=RECEBEU_CHOICES, default=u'Sim', max_length=3)

    # Fields de auditoria
    status = models.BooleanField(u'Ativo?', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'fornecimentos'
        verbose_name = u'Fornecimento'
        verbose_name_plural = u'Fornecimentos'

    def pontuacao(self):
        return (self.quantidade +
                self.prazo + self.qualidade +
                self.qualidade + self.integridade +
                self.nota_fiscal)

    def data_formatada(self):
        dt = self.data
        df = DateFormat(dt)
        return df.format('d/m/Y')
