# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django import forms
from .models import *


class FornecimentoForm(ModelForm):
    class Meta:
        model = Fornecimento
        exclude = ['status']
        widgets = {
            'ug': forms.Select(attrs={'class': 'form-control-middle'}),
            'fornecedor': forms.Select(attrs={'class': 'form-control-middle'}),
            'data': forms.DateTimeInput(attrs={'class': 'form-control-middle'}),
            'empenho': forms.TextInput(attrs={'class': 'form-control'}),
            'nf': forms.TextInput(attrs={'class': 'form-control-middle'}),
            'quantidade': forms.Select(attrs={'class': 'form-control-middle'}),
            'prazo': forms.Select(attrs={'class': 'form-control-middle'}),
            'qualidade': forms.Select(attrs={'class': 'form-control-middle'}),
            'integridade': forms.Select(attrs={'class': 'form-control-middle'}),
            'nota_fiscal': forms.Select(attrs={'class': 'form-control-middle'}),
            'obs': forms.Textarea(attrs={'class': 'form-control', 'rows': '3'}),
            'recebeu': forms.Select(attrs={'class': 'form-control-quatle'}),
        }
