# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class UnidadeGestora(models.Model):
    cnpj = models.CharField(u'CNPJ', max_length=50, unique=True)
    nome = models.CharField(u'Nome', max_length=200)
    uf = models.CharField(u'UF', max_length=2)
    cidade = models.CharField(u'Cidade', max_length=150)
    responsavel = models.CharField(u'Responsável', max_length=200)
    telefone = models.CharField(u'Telefone', max_length=30)
    obs = models.TextField(u'Obs', blank=True, null=True)

    # Fields de auditoria
    status = models.BooleanField(u'Ativo?', default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'unidade_gestora'
        verbose_name = u'Unidade Gestora'
        verbose_name_plural = u'Unidades Gestoras'

    def __str__(self):
        return self.nome

    def __unicode__(self):
        return self.nome
