# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from .forms import *
from django.shortcuts import render
from django.contrib.auth.decorators import login_required


# Unidades Gestoras
@login_required(login_url='/entrar/')
def cadastrar_ug(request):
    form = UnidadeGestoraForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_ugs))

    return render(request, 'cadastrar_ug.html', {'form': form, 'request': request})


@login_required(login_url='/entrar/')
def lista_ugs(request):
    unidades_gestoras = UnidadeGestora.objects.filter(status=True)
    return render(request, 'unidades_gestoras.html', {'unidades_gestoras': unidades_gestoras})


@login_required(login_url='/entrar/')
def ugs_cancelados(request):
    unidades_gestoras = UnidadeGestora.objects.filter(status=False)
    return render(request, 'ugs_cancelados.html', {'unidades_gestoras': unidades_gestoras})


@login_required(login_url='/entrar/')
def exclui_ug(request, id):
    unidade_gestora = UnidadeGestora.objects.get(pk=id)
    unidade_gestora.status = False
    unidade_gestora.save()
    return HttpResponseRedirect(reverse(lista_ugs))


@login_required(login_url='/entrar/')
def ativar_ug(request, id):
    unidade_gestora = UnidadeGestora.objects.get(pk=id)
    unidade_gestora.status = True
    unidade_gestora.save()
    return HttpResponseRedirect(reverse(ugs_cancelados))


@login_required(login_url='/entrar/')
def editar_ug(request, id):
    unidade_gestora= UnidadeGestora.objects.get(pk=id)
    form = UnidadeGestoraForm(request.POST or None, instance=unidade_gestora)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse(lista_ugs))
    return render(request, 'editar_ug.html', {'form': form})