# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from .models import *


class UnidadeGestoraForm(ModelForm):
    class Meta:
        model = UnidadeGestora
        exclude = ['']
        widgets = {
            'cnpj' : forms.TextInput(attrs = {'class' : 'form-control', 'placeholder': u'99.999.999/9999-99'}),
            'nome': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Ex: Alimentos S/A'}),
            'cidade': forms.Select(attrs={'class': 'form-control', 'placeholder': u'Ex: Recife'}),
            'uf': forms.Select(attrs={'class': 'form-control', 'placeholder': 'Ex: PE'}),
            'responsavel': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'Ex: João Silva'}),
            'telefone': forms.TextInput(attrs={'class': 'form-control', 'placeholder': u'(99) 9999-9999'}),
            'obs': forms.Textarea(attrs={'class': 'form-control', 'rows': '3'})
        }