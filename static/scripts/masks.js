$(document).ready(function(){
    $("#id_data").mask("99/99/9999");
    $("#id_cnpj").mask("99.999.999/9999-99");
    $("#id_telefone").mask("(99) 9999-9999");
    $("#id_celular").mask("(99) 99999-9999");
    $("#id_cep").mask("99999-999");
});