
//morris area chart

$(function () {

    //morris line chart
    Morris.Line({
        element: 'morris-line-chart',
        data: [{
            m: 'Jan',
            a: 10,
            b: 90,
            c: c
        },{
            m: 'Fev',
            a: 60,
            b: 25,
            c: c1
        },{
            m: 'Mar',
            a: 33,
            b: 55,
            c: 25
        },{
            m: 'Abr',
            a: 22,
            b: 58,
            c: 25
        },{
            m: 'Mai',
            a: 14,
            b: 25,
            c: 25
        },{
            m: 'Jun',
            a: 73,
            b: 82,
            c: 25
        },{
            m: 'Jul',
            a: 10,
            b: 2,
            c: 25
        },{
            m: 'Ago',
            a: 58,
            b: 74,
            c: 25
        },{
            m: 'Set',
            a: 1,
            b: 90,
            c: 25
        },{
            m: 'Out',
            a: 55,
            b: 80,
            c: 25
        },{
            m: 'Nov',
            a: 20,
            b: 70,
            c: 25
        },{
            m: 'Dez',
            a: 100,
            b: 90,
            c: 25
        }],
        xkey: 'm',
        ykeys: ['a', 'b', 'c'],
        labels: ['Masterboi', 'Friboi', 'Mafisa'],
        hideHover: 'auto',
        parseTime: false,
        pointSize: 8,
        resize: true
    });

});
