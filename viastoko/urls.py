# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from apps.website.views import *
from apps.fornecedor.views import *
from apps.unidade_gestora.views import *
from apps.fornecimento.views import *
from apps.usuario.views import *
from apps.relatorio.views import *

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^dashboard/$', dashboard, name='dashoard'),
    url(r'^admin/', include(admin.site.urls)),

    # fornecedores
    url(r'^cadastrar_fornecedor/$', cadastrar_fornecedor, name='Cadastro de Fornecedores'),
    url(r'^lista_fornecedores/$', lista_fornecedores, name='Listagem de Fornecedores'),
    url(r'^fornecedores_cancelados/$', fornecedores_cancelados, name='Fornecedores Cancelados'),
    url(r'^exclui_fornecedor/(?P<id>\d+)/$', exclui_fornecedor, name='Excluir Fornecedor'),
    url(r'^ativar_fornecedor/(?P<id>\d+)/$', ativar_fornecedor, name='Ativar Fornecedor'),
    url(r'^editar_fornecedor/(?P<id>\d+)/$', editar_fornecedor, name='Editar Fornecedor'),

    # Segmentos para fornecedores
    url(r'^cadastrar_segmento/$', cadastrar_segmento, name='Cadastro de Segmentos'),
    url(r'^lista_segmentos/$', lista_segmentos, name='Listagem de Segmentos'),
    url(r'^segmentos_cancelados/$', segmentos_cancelados, name='Segmentos Cancelados'),
    url(r'^exclui_segmento/(?P<id>\d+)/$', exclui_segmento, name='Excluir Segmento'),
    url(r'^ativar_segmento/(?P<id>\d+)/$', ativar_segmento, name='Ativar Segmento'),
    url(r'^editar_segmento/(?P<id>\d+)/$', editar_segmento, name='Editar Segmento'),

    # Unidades Gestoras
    url(r'^cadastrar_ug/$', cadastrar_ug, name='Cadastro de Unidade Gestora'),
    url(r'^lista_ugs/$', lista_ugs, name='Listagem de Unidades Gestoras'),
    url(r'^ugs_cancelados/$', ugs_cancelados, name='Unidades Gestoras Cancelados'),
    url(r'^exclui_ug/(?P<id>\d+)/$', exclui_ug, name='Excluir Unidade Gestora'),
    url(r'^ativar_ug/(?P<id>\d+)/$', ativar_ug, name='Ativar Unidade Gestora'),
    url(r'^editar_ug/(?P<id>\d+)/$', editar_ug, name='Editar Unidade Gestora'),

    # Fornecimentos
    url(r'^cadastrar_fornecimento/$', cadastrar_fornecimento, name='Cadastro de Fornecimento'),
    url(r'^lista_fornecimentos/$', lista_fornecimentos, name='Listagem de Fornecimentos'),
    # url(r'^criterios_cancelados/$', criterios_cancelados, name='Criterios Cancelados'),
    # url(r'^exclui_criterio/(?P<id>\d+)/$', exclui_criterio, name='Excluir Criterio'),
    # url(r'^ativar_criterio/(?P<id>\d+)/$', ativar_criterio, name='Ativar Criterio'),
    # url(r'^editar_criterio/(?P<id>\d+)/$', editar_criterio, name='Editar Criterio'),

    # Usuarios
    url(r'^cadastrar_usuario/$', cadastrar_usuario, name='Cadastrar Usuario'),
    url(r'^listar_usuarios/$', listar_usuarios, name='Listar Usuarios'),
    url(r'^usuarios_cancelados/$', usuarios_cancelados, name='Listar Usuarios Cancelados'),
    url(r'^entrar/$', entrar, name='entrar'),
    url(r'^sair/$', sair, name='Sair'),
    url(r'^editar_informacoes/(?P<id>\d+)/$', editar_informacoes, name='editar informacoes'),
    url(r'^exclui_usuario/(?P<usuario_id>\d+)/$', exclui_usuario, name='Excluir Usuario'),

    # Relatórios
    url(r'^por_segmento/(?P<segmento>\d+)/$', por_segmento, name='Ranking por segmento'),
    url(r'^por_segmento_geral/$', por_segmento_geral, name='Ranking por segmento'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
